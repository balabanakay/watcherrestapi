create database postdb;
	
CREATE USER 'postuser'@'localhost' IDENTIFIED BY 'postpwd';
	
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP, REFERENCES
ON postdb.*
TO 'postuser'@'localhost' ;
