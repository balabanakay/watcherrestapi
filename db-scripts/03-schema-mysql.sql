USE postdb;

--DROP TABLE IF EXISTS watch_job ;
CREATE TABLE IF NOT EXISTS watch_job  (
	id BIGINT NOT NULL PRIMARY KEY,
    start_time VARCHAR(120),
    interval_log BIGINT,
    file_path VARCHAR(120)
);

CREATE TABLE IF NOT EXISTS scheduler_job  (
	id BIGINT NOT NULL PRIMARY KEY,
    start_time VARCHAR(120)
);


CREATE TABLE IF NOT EXISTS wlog  (
    severity VARCHAR (10)
);


CREATE TABLE IF NOT EXISTS post  (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    severity_type VARCHAR (10),
	severity_count BIGINT,
    PRIMARY KEY (id)
);
