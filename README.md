# Status Monitoring of a Web Site (Java Backend )
## RESTful Service Using Spring Batch Job and Scheduler

This project provides Java Backend for a web site log monitoring. 

Simply clone the project or download and extract the .zip to get started. 

To be able to use his project; it is required to download and install frontend site [angular-status2]([https://bitbucket.org/balabanakay/angular-status2]) project.

## Concepts Covered

* Read a log file via Spring Batch Job to find out the severity message type counts within specified interval.
* Schedule a trigger for every 500 milliseconds to check if the specified interval time is elapsed or not. If the specified interval is elapsed trigger a Spring Batch Job.
* Publish the results of the Spring Batch Job as Restful service. 
The Restful service end points are :
[posts](http://localhost:9000/api/dataservice/posts) 
[intervals](http://localhost:9000/api/dataservice/intervals)
* To be able to update the interval setting the fallowing REST end point is being used:
[single interval](http://localhost:9000/api/dataservice/interval/1)
* Check the Restful service end points by Postman.


## Software Requirements To Run Locally 

* Java SDK 8
* MySQL DB 
* Maven 3 

## Running the Application Locally

1. Install Java 8 SDK on your dev box, if you are not done before.

1. Install Apache Maven 3 on your dev box, if you are not done before.

1. Install MySql DB on your dev box, if you are not done before. You can also need MySql Workbench to run db scripts. You can also your own choose DB tool.

1. Run db scripts from [db-scripts](https://bitbucket.org/balabanakay/watcherrestapi/src/bdcbe58bc30379f8facdc6847366bb11a45f0328/db-scripts/?at=master) folder.

1. Create a database in MySql by using [01-create-postdb-schema-sql](https://bitbucket.org/balabanakay/watcherrestapi/src/bdcbe58bc30379f8facdc6847366bb11a45f0328/db-scripts/01-create-postdb-schema.sql?at=master&fileviewer=file-view-default).

1. Login to MySql with user :`postuser` and password : `postpwd`. Run the remaining 3 db-scritps at db-scripts folder : [02-spring-batch-create-tables-mysql.sql](https://bitbucket.org/balabanakay/watcherrestapi/src/bdcbe58bc30379f8facdc6847366bb11a45f0328/db-scripts/02-spring-batch-create-tables-mysql.sql?at=master&fileviewer=file-view-default) [03-schema-mysql.sql](https://bitbucket.org/balabanakay/watcherrestapi/src/bdcbe58bc30379f8facdc6847366bb11a45f0328/db-scripts/03-schema-mysql.sql?at=master&fileviewer=file-view-default) [04-data-mysql.sql](https://bitbucket.org/balabanakay/watcherrestapi/src/bdcbe58bc30379f8facdc6847366bb11a45f0328/db-scripts/04-data-mysql.sql?at=master&fileviewer=file-view-default)

1. Change the log file destination at DB table `WATCH_JOB` file_path column accordingly. The path could be given relative to the project source folder. The sample file is located at ./src/main/resources/log/output.log.

1. Clone this project.

## Testing The application

* Let’s run the application[from IDE or on command line]. To test this API, I will use an external client POSTMAN (An extension from CHROME).
Apart from IDE, you can also run this app using following approaches.

* java -jar path-to-jar
* on Project root , mvn spring-boot:run

## Explanation of Scheduler Algorithm

* In SchedulerTask.java A fixed time trigger will be run. This trigger is set to 500 milliseconds. This trigger will check in every 500 milliseconds, if enough time is elapsed or not to start a batch job. A new Batch job will start, if the set interval time is elapsed.

* * For example if the initial start time is  '2017-03-09 17:00:00.001' and the interval is set to 5 seconds (5000 milliseconds) next batch job will start at '2017-03-09 17:05:001. The mentioned trigger will be triggered 10 times =>  10* 500 = 5000 milliseconds till a  new batch Job will be initiated. 

* In SchedulerTask. java, first the scheduler will read the scheduler_job table, if the scheduler start time is null it will start a new batch job and insert a new record to scheduler_job table as the real world time. For the next triggers it will calculate the elapsed time. the calculation is scheduler last  start time + real elapsed time. if the calculated time is less than real world time it will do nothing. If the calculated time is bigger and equal to  real world time start a new batch job; update the scheduler start time as real world time.

## Explanation of Spring Batch Job Algorithm

* Step1:
* reader : Read from a flat file and limit the number of records to the filtered rows. To be able to gain from memory consumption, this function reads only the rows within interval. The interval is given by job parameters. 
* writer : Write the wLog objects to the W_LOG table. The used sql as fallows : `INSERT INTO wlog (severity) VALUES (:severityType)`

* * the outputs can be `INFO`, `INFO`, `WARNING`, `INFO`, `WARNING`, `INFO`, `INFO`

* listener : BeforeStep :truncate WLOG table in the DB. AfterStep :compare the startDT with the job parameters if they are different update the value in the DB. Replace the value in the DB with the value in Job Parameters.

* Step2:
* reader : read the severity counts group by severity types from DB. `SELECT severity, COUNT(severity) FROM wlog GROUP BY severity`
* writer : write the post results into the db via this sql : `INSERT INTO post (severity_type, severity_count) VALUES (:severityType, :severityCount)`
The results can be as fallows :

* * INFO :8
* * WARNING :1 
* * P.S. There is no ERROR line. Because there is no severity type messages within the given interval.

* listener : BeforeStep :truncate POST table in DB before execution of the step. AfterStep :return ExitStatus.COMPLETED
*  
## REST Api end points
* Posts service endpoints 
it is only allowed to get , the posts are not allowed to update, delete or add (read only)
* get : http://localhost:9000/api/dataservice/posts
* get : http://localhost:9000/api/dataservice/post/1

* Intervals service endpoints 
it is allowed to read and update the interval.
* get : http://localhost:9000/api/dataservice/intervals
* get : http://localhost:9000/api/dataservice/interval/1
it is only allowed to update the interval service with put operator
* put : http://localhost:9000/api/dataservice/interval/1