package kulaksiz.batur.batch;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;

import kulaksiz.batur.batch.model.LogItem;
import kulaksiz.batur.batch.model.Severity;
import kulaksiz.batur.batch.model.WLog;

public class LogItemCustomReader implements ItemReader<WLog>{

	private static final Logger log = LoggerFactory.getLogger(LogItemCustomReader.class);
	
	private List<WLog> logList;
	private int nextLogIndex;
	
	private boolean amIEntered = false;
	private boolean amIExit = false;
	private boolean continueTo = true;
	
	private String dateTimeFormat;
	private DateTimeFormatter formatter ;

	private String startDT; 
	private LocalDateTime startLDT;
	// interval in seconds ;
	private Long interval;
	private LocalDateTime endLDT;
	
	private String resource;


	/*
	 * Read from a flat file and limit the number of records.
	 * only read the filtered rows.
	 * to be able to gain from memory consumption, this function reads 
	 * only the rows within interval. The interval is given by job parameters
	 * amIEntered : the first time we encounter the starting datetime
	 * amIExit : the first time we encounter we passed the ending datetime
	 * 
	 */
	public void filterData () throws IOException{
		this.nextLogIndex =0;
		this.logList = new ArrayList <WLog> ();
		FileInputStream inputStream = null;
		Scanner sc = null;
		try {

			inputStream = new FileInputStream(resource);
			sc = new Scanner(inputStream, "UTF-8");
			while (sc.hasNextLine() && continueTo ) {

				if   ( amIEntered   &&   amIExit){
					this.continueTo = false;
				} //if

				String line = sc.nextLine();
				LogItem logItem = this.parseLogItem(line);
				if (logItem != null){
					final LocalDateTime  ldt = logItem.getLocalDateTime() ;

					if (ldt.isAfter(this.getEndLDT())){
						amIExit = true;
						break;
					}

					if ( ldt.isAfter(startLDT)  && ldt.isBefore(this.getEndLDT())){

						if (!amIEntered ){
							amIEntered = true;
						}

						WLog wLog = new WLog(logItem.getSeverity());
						logList.add(wLog);
						//log.debug("logItem :" + logItem.getLocalDateTime());
					} //if
				}
				// note that Scanner suppresses exceptions
			}//while
		}catch (IOException ex){
			log.error(ex.getLocalizedMessage());

		}catch (NullPointerException ex1){
			log.error(ex1.getLocalizedMessage());

		}
		finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (sc != null) {
				sc.close();
			}
		}
	}


	@Override
	public WLog read() throws Exception {
		//LogItem nextLogItem = null;
		WLog wLog =null;
		if (this.logList == null){
			filterData();
		}

		if (nextLogIndex < logList.size()){
			wLog = logList.get(nextLogIndex);
			nextLogIndex++;
		}

		return wLog;
	}

	/* 
	 * Parse the line from FlatFile as a LogItem object
	 * 
	 * //2017-03-04 17:59:41,253 ERROR Some error message
	 */
	public LogItem parseLogItem(String line){


		LogItem logItem =null;
		try{
			String strLdt = line.substring(0,23);
			logItem = new LogItem();
			logItem.setLocalDateTime(LocalDateTime.parse(strLdt, formatter));
			Character messageTypeChar = line.charAt(24);
			switch (messageTypeChar) {
			case 'I':
				logItem.setSeverity(Severity.INFO);
				break;
			case 'W':
				logItem.setSeverity(Severity.WARNING);
				break;
			case 'E':
				logItem.setSeverity(Severity.ERROR);
				break;
			default:
				break;
			}

		}catch (Exception ex){
			log.error("Error in Parsing Log Item" + ex.getMessage());
			return null;
		}
		return logItem;
	}



	public void setDateTimeFormat(String dateTimeFormat) {
		this.dateTimeFormat = dateTimeFormat;
		this.formatter = DateTimeFormatter.ofPattern(this.dateTimeFormat);

	}

	public void setStartDT(String startDT) {
		this.startDT = startDT;
		this.startLDT = LocalDateTime.parse(this.startDT, formatter);
	}

	public void setInterval(Long interval) {
		this.interval = interval;
	}

	public LocalDateTime getEndLDT() {
		if (this.endLDT == null){
			this.setEndLDT();
		}
		return endLDT;
	}

	public void setEndLDT() {

		this.endLDT = this.startLDT.plusSeconds(interval.intValue());
	}


	public String getResource() {
		return resource;
	}


	public void setResource(String resource) {
		this.resource = resource;
	}




}
