package kulaksiz.batur.batch.model;

import java.time.LocalDateTime;

public class LogItem {
	
	private LocalDateTime localDateTime;
	private Severity severity;
	String message;
	
	private static final long serialVersionUID = 6648416741847674063L;

	/*
	 * It will act as one line the log file
	 * For example: 2017-03-04 17:00:03,080 INFO Some info message
	 * it consists 3 properties
	 * localDateTime, Severity, message
	 */
	public LogItem() {

	}
	

	public LogItem(LocalDateTime localDateTime, Severity severity, String message) {
		super();
		this.localDateTime = localDateTime;
		this.severity = severity;
		this.message = message;
	}




	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}


	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}


	public Severity getSeverity() {
		return severity;
	}

	public void setSeverity(Severity severity) {
		this.severity = severity;
	}
	public void setSeverity(String severe) {
		switch(severe.toUpperCase()){
		case "INFO":
			this.severity = Severity.INFO;
			break;
		case "WARNING":
			this.severity = Severity.WARNING;
			break;
		case "ERROR":
			this.severity = Severity.ERROR;
			break;
		default:
			this.severity = Severity.INFO;
			break;
		}
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	@Override
	public String toString() {
		return "LogItem [localDateTime=" + localDateTime + ", severity=" + severity + ", message=" + message + "]";
	}

	

}
