package kulaksiz.batur.batch.model;

public class WLog {
	
	private String severityType;

	/*
	 * It represent one row in the WLOG table
	 * severityType
	 */
	public WLog() {

	}
	
	public WLog(Severity severity) {
		setSeverityType(severity);
	}



	public String getSeverityType() {
		return severityType;
	}

	public void setSeverityType(Severity severity) {
		this.severityType = severity.name();
	}

	@Override
	public String toString() {
		return "WLog [severityType=" + severityType + "]";
	}
	
	

}
