package kulaksiz.batur.batch.model;

public class SeverityCount {
	
	private String severityType;
	private int severityCount;
	
	/*
	 * It represents one row in POST table
	 * severityType and SeverityCount
	 * 
	 */
	public SeverityCount() {
		
	}
	public SeverityCount(String severityType, int severityCount) {
		
		this.severityType = severityType;
		this.severityCount = severityCount;
	}
	
	public String getSeverityType() {
		return severityType;
	}
	public void setSeverityType(String severityType) {
		this.severityType = severityType;
	}
	public int getSeverityCount() {
		return severityCount;
	}
	public void setSeverityCount(int severityCount) {
		this.severityCount = severityCount;
	}
	@Override
	public String toString() {
		return "WLogCount [severityType=" + severityType + ", severityCount=" + severityCount + "]";
	}
	

}
