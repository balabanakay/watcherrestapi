package kulaksiz.batur.batch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class StepListener implements StepExecutionListener {

	private static final Logger log = LoggerFactory.getLogger(StepListener.class);

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	private String truncateWLogTableQuery;
	private String readJobParametersQuery;
	private String updateJobParametersQuery;
	
	
	
	/*
	 * Before execution of the step:
	 * Truncate WLOG table in the DB   
	 * (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public void beforeStep(StepExecution arg0) {
		
		log.debug("Truncating the table WLOG in DB");
		this.jdbcTemplate.execute(this.truncateWLogTableQuery);
	}
	
	
	
	/*
	 *  After execution of the step:
	 *  compare the startDT with the job parameters if they are different update the value in the DB
	 *  (replace the value in the DB with the value in Job Parameters).
	 */ 	 
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		JobParameters parameters = stepExecution.getJobExecution().getJobParameters(); 
		Map <String, JobParameter> parameterMap = parameters.getParameters();
		JobParameter oJobParameter = parameterMap.get("START_DT");
		
		
		String startDT = "" + oJobParameter.getValue();
		log.debug("startDT :" + startDT);	
	
		
		Map <String, String> namedParameters = new HashMap<String, String> ();   
	    
	   
		//Get the startTime from DB
		String dbStartDT = "";
		List<String> results = jdbcTemplate.query(this.readJobParametersQuery, new RowMapper<String>() {
				@Override
				public String mapRow(ResultSet rs, int row) throws SQLException {
					return new String(rs.getString(1));
				}
		});
		
		for (String str : results) {
			dbStartDT = str;
		}
				
		// if they are not equal update the table value
		if (!startDT.equalsIgnoreCase(dbStartDT)){
			log.debug("Updating the Starting Time in DB");
			namedParameters.put("startTime", startDT);
			namedParameterJdbcTemplate.update(this.updateJobParametersQuery,namedParameters );
		}
		return ExitStatus.COMPLETED;
	}


	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public void setTruncateWLogTableQuery(String truncateWLogTableQuery) {
		this.truncateWLogTableQuery = truncateWLogTableQuery;
	}
	
	public void setReadJobParametersQuery(String readJobParametersQuery) {
		this.readJobParametersQuery = readJobParametersQuery;
	}

	public void setUpdateJobParametersQuery(String updateJobParametersQuery) {
		this.updateJobParametersQuery = updateJobParametersQuery;
	}
	
	

}
