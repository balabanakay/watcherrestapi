package kulaksiz.batur.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import kulaksiz.batur.batch.model.SeverityCount;

public class SeverityCountMapper implements RowMapper<SeverityCount>{

	/*
	 * Map the returning resulset as a Severity Count object
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	public SeverityCount mapRow(ResultSet rs, int rowNum) throws SQLException {
		SeverityCount severityCount = new SeverityCount();
		severityCount.setSeverityType(rs.getString(1));
		severityCount.setSeverityCount(rs.getInt(2));
		
		return severityCount;
	}

}
