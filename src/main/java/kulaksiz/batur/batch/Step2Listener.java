package kulaksiz.batur.batch;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.jdbc.core.JdbcTemplate;

public class Step2Listener implements StepExecutionListener {

	private static final Logger log = LoggerFactory.getLogger(Step2Listener.class);

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	private String truncateQuery;
	

	/*
	 * Truncate POST table in DB vefore execution of the step
	 * (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public void beforeStep(StepExecution arg0) {

		log.debug("Truncating the table POST in DB");
		this.jdbcTemplate.execute(this.truncateQuery);
	}
	
	
	/*
	 * after completion of the Step return ExitStatus.COMPLETED
	 * 	(non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
			return ExitStatus.COMPLETED;
	}

	
	
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	public void setTruncateQuery(String truncateQuery) {
		this.truncateQuery = truncateQuery;
	}

	
	

}
