/**
 * Spring Boot Rest Api Application
 * Main class
 */
package kulaksiz.batur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


//@SpringBootApplication(scanBasePackages={"kulaksiz.batur.rest"})// same as @Configuration @EnableAutoConfiguration @ComponentScan combined
@SpringBootApplication
@EnableScheduling
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
