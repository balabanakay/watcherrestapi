package kulaksiz.batur.schedule;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kulaksiz.batur.schedule.model.SBParam;
import kulaksiz.batur.schedule.model.SHParam;
import kulaksiz.batur.schedule.repository.SBParamRepository;
import kulaksiz.batur.schedule.repository.SHParamRepository;

@Component
public class ScheduledTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
   
    private String dateTimeFormat = "yyyy-MM-dd HH:mm:ss,SSS";
    private DateTimeFormatter formatter ;
    
    {
    	this.formatter = DateTimeFormatter.ofPattern(this.dateTimeFormat);
    	
    }
    
    private String nextScheduledTime = null;
    
    @Autowired
    SBParamRepository sbRepository;
    
    @Autowired
    SHParamRepository shRepository;

    /*
     * Trigger it in every 500 milliseconds
     * This, will check in every 500 milliseconds if it is enough time elapsed to 
     * start a batch job. A new Batch job will start in interval time.
     * For example if the initial start time is  '2017-03-09 17:00:00.001' and the interval is
     * set to 5 seconds (5000 milliseconds) next Batch Job will start at '2017-03-09 17:05:001'
     * This will be triggered 10 times =>  10* 500 = 5000 milliseconds till a  new Batch Job 
     * will be initiated.  
     * 
     *  Pls. NOTE :increasing the start date time is scheduler's responsibility
     * 
     */
    @Scheduled(fixedRate = 500)
    public void triggerSpringBatchJob() {
    	
        log.debug("The time is now {}", dateFormat.format(new Date()));
        
        // read Spring Batch Job parameters
        SBParam sbParam =sbRepository.findOne(1L);
        //log.info(sbParam.getStartTime());
        
        // read Scheduled Job Parameters from DB
        SHParam shParam =null;
        try{
        	shParam = shRepository.findOne(1L);
        }catch (NullPointerException ex){
        	log.debug("first time creating shParam");
        }
        // if this is the first time, which is null in the DB 
        // or the value has remained from an old run 
        // then set it to current localDateTime.
        if (null == shParam){
        	shParam = new SHParam (1L, LocalDateTime.now().toString());
        	shRepository.save(shParam);
        }
        
        //calculate startTime + interval
        LocalDateTime shStartTime = this.parseLocalDateTime(shParam.getStartTime());
        LocalDateTime calcTime = shStartTime.plusSeconds(sbParam.getIntervalLog());
        LocalDateTime now = LocalDateTime.now();
        // if there is elapsed time or if it is the first time
        // initiate a new Spring Batch Job 
        if (calcTime.isBefore(now) ) {
        	
        	log.info("Launching a new Batch Job at time : "+ now.toString());
        	String logFilePath = sbParam.getFilePath();
        	String startTime = sbParam.getStartTime();
        	Long interval = sbParam.getIntervalLog();
        	
        	if (this.nextScheduledTime == null){
        		this.nextScheduledTime = startTime;
        	}
        	
        	this.startANewJob(logFilePath, this.nextScheduledTime, interval, now);
        	shParam.setStartTime(now.toString());
        	shRepository.save(shParam);
        	
        	this.incrementScheduledJobTime(nextScheduledTime, interval);
        	
        }else{
        	//do Nothing
        }
        
    }
    
    /*
     * 	Starting a new Spring Batch Job with related Job Paremeters.
     *  Pls. NOTE :increasing the start date time is scheduler's responsibility
     */
    public void startANewJob(String logFilePath, String startTime , Long interval, LocalDateTime now){
    	
    	//2017-03-04 17:00:01,717
    	String[] springConfig  = 
			{	"watcherJob.xml" };
		
		ApplicationContext context = 
				new ClassPathXmlApplicationContext(springConfig);
		
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job job = (Job) context.getBean("watcherJob");

		try {

			JobExecution execution = jobLauncher.run(
					job,
					new JobParametersBuilder()
						.addString("LOG_FILE_PATH", logFilePath)
						.addString("START_DT", startTime)
						.addLong("INTERVAL", interval)
						.addString ("DATE", now.toString() )
						.toJobParameters()
				);
			log.debug("Exit Status : " + execution.getStatus());

		} catch (Exception e) {
			e.printStackTrace();
		}

		log.debug("Done");
    }
    
    /*
     * increment scheduling time
     * increasing the start date time is scheduler's responsibility
     */
    public void incrementScheduledJobTime (String startTime, Long interval ){
    	LocalDateTime ldt = this.parseLocalDateTime(startTime);
    	LocalDateTime nextSchTime = ldt.plusSeconds(interval);
    	this.nextScheduledTime = this.convertToDateTimeString(nextSchTime);
    }
    
    /*
     * Parse Project time format and convert it into Java 8 LocalDateTime format
     * Project time format: 	2017-03-04 17:00:01,717
     * LocalDateTime format:	2017-03-09T18:19:30.529
     * Change characters 'T' and '.' accordingly
     */	
    public LocalDateTime parseLocalDateTime(String line){
    	StringBuilder sb = new StringBuilder(line);
    	sb.setCharAt(10, ' ');
    	sb.setCharAt(19, ',');
		LocalDateTime ldt =LocalDateTime.parse(sb, formatter);
		return ldt;
	}
    
   
    /*
     * convert LocalDateTime object To String
     */
    public String convertToDateTimeString (LocalDateTime ldt){
    	StringBuilder sb = new StringBuilder(ldt.toString());
    	sb.setCharAt(10, ' ');
    	sb.setCharAt(19, ',');
		return sb.toString();
	}

    
}
