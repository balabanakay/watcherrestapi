package kulaksiz.batur.schedule.repository;

import org.springframework.data.repository.CrudRepository;

import kulaksiz.batur.schedule.model.SHParam;

/*
 * Spring Data Repository for SCHEDULER_JOB table's data object.
 * use all the default CRUD operations
 */
public interface SHParamRepository extends CrudRepository<SHParam, Long>{

}
