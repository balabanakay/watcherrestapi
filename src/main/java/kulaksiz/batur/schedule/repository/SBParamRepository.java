package kulaksiz.batur.schedule.repository;

import org.springframework.data.repository.CrudRepository;

import kulaksiz.batur.schedule.model.SBParam;

/*
 * Spring Data Repository for WATCH_JOB table's data object.
 * use all the default CRUD operations
 */
public interface SBParamRepository extends CrudRepository<SBParam, Long>{

	
}
