package kulaksiz.batur.schedule.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SCHEDULER_JOB")
public class SHParam {
	
	@Id
	private long id;
	private String startTime;

	
	/*
	 * Represent a row in the SCHEDULER_JOB table.
	 * This table persists the last time which the scheduled job has been initiated in
	 * current real time.
	 */
	public SHParam() {

	}

	public SHParam(long id, String startTime) {
		this.id = id;
		this.startTime = startTime;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getStartTime() {
		return startTime;
	}


	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}


	@Override
	public String toString() {
		return "shParam [id=" + id + ", startTime=" + startTime + "]";
	}
	
	

}
