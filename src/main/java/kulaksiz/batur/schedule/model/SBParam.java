package kulaksiz.batur.schedule.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="WATCH_JOB")
public class SBParam {
	
	@Id
	private long id;
	

	private String startTime;
	private long intervalLog;
	private String filePath;
	
	/*
	 * Represent a row in the WATCH_JOB table.
	 */
	public SBParam() {

	}
	public SBParam(long id, String startTime, long intervalLog, String filePath) {
		super();
		this.id = id;
		this.startTime = startTime;
		this.intervalLog = intervalLog;
		this.filePath = filePath;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public long getIntervalLog() {
		return intervalLog;
	}
	public void setIntervalLog(long intervalLog) {
		this.intervalLog = intervalLog;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	@Override
	public String toString() {
		return "SBParam [id=" + id + ", startTime=" + startTime + ", intervalLog=" + intervalLog + ", filePath="
				+ filePath + "]";
	}
	

}
