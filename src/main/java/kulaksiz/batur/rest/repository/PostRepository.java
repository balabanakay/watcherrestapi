package kulaksiz.batur.rest.repository;

import org.springframework.data.repository.CrudRepository;

import kulaksiz.batur.rest.model.Post;

/*
 * Spring Data Repository for Post
 * use all the default CRUD operations
 */
public interface PostRepository extends CrudRepository<Post, Long> {

    
}