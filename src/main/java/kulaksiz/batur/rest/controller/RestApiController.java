package kulaksiz.batur.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kulaksiz.batur.rest.model.Post;
import kulaksiz.batur.rest.repository.PostRepository;
import kulaksiz.batur.rest.util.CustomErrorType;

//@CrossOrigin (origins = "http://localhost:3000", maxAge=3600)
@CrossOrigin 
@RestController
@RequestMapping("api/dataservice")
public class RestApiController {

	public static final Logger log = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	PostRepository postRepository; //Service which will do all data retrieval/manipulation work

	/*
	 *  			Retrieve All Posts
	 */
	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	public ResponseEntity<List<Post>> listAllPosts() {

		Iterable<Post> source = postRepository.findAll();
		List<Post> posts = new ArrayList<>();
		source.forEach(posts::add);

		if (posts.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Post>>(posts, HttpStatus.OK);
	}


	/*
	 * 				Retrieve Single Post
	 */
	@RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getPost(@PathVariable("id") long id) {
		log.info("Fetching Post with id {}", id);

		Post target = postRepository.findOne(id);
		if (target == null) {
			log.error("Post with id {} not found.", id);
			return new ResponseEntity (new CustomErrorType("Post with id " + id 
					+ " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Post>(target, HttpStatus.OK);
	}




}