package kulaksiz.batur.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kulaksiz.batur.rest.model.Interval;
import kulaksiz.batur.rest.util.CustomErrorType;
import kulaksiz.batur.schedule.model.SBParam;
import kulaksiz.batur.schedule.repository.SBParamRepository;

@CrossOrigin
@RestController
@RequestMapping("api/dataservice")
public class RestApiController2 {

	public static final Logger log= LoggerFactory.getLogger(RestApiController2.class);

	@Autowired
	SBParamRepository sBParamRepository;

	/*
	 *  				Retrieve All Intervals	
	 */
	@RequestMapping(value = "/intervals", method = RequestMethod.GET)
	public ResponseEntity<List<Interval>> listAllSBParams() {

		Iterable<SBParam> source = sBParamRepository.findAll();
		List<SBParam> sBParamList = new ArrayList<>();

		source.forEach(sBParamList::add);

		if (sBParamList.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		List<Interval> intervalList = new ArrayList<>();
		for (SBParam sBParam: sBParamList){
			intervalList.add(new Interval (sBParam.getId(), sBParam.getIntervalLog()));
		}

		return new ResponseEntity<List<Interval>>(intervalList, HttpStatus.OK);
	}


	/*
	 *                Retrieve Single Interval
	 */
	@RequestMapping(value = "/interval/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getSBParam(@PathVariable("id") long id) {

		log.info("Fetching SBParam with id {}", id);
		SBParam target = sBParamRepository.findOne(id);
		if (target == null) {
			log.error("SBParam with id {} not found.", id);
			return new ResponseEntity (new CustomErrorType("SBParam with id " + id 
					+ " not found"), HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Interval>(new Interval(target.getId(), target.getIntervalLog()), HttpStatus.OK);
	}


	/*
	 *  			Update an Interval Object
	 */
	@RequestMapping(value = "/interval/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateSBParam(@PathVariable("id") long id, @RequestBody Interval interval) {

		log.info("Updating SBParam with id {}", id);
		SBParam target = sBParamRepository.findOne(id);
		if (target == null) {
			log.error("Unable to update. Interval with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to upate. Interval with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		target.setIntervalLog(interval.getIntervalValue());
		SBParam currentSBParam =sBParamRepository.save(target);

		return new ResponseEntity<Interval>(new Interval(currentSBParam.getId(), currentSBParam.getIntervalLog()), HttpStatus.OK);
	}

}