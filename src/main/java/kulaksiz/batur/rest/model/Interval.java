package kulaksiz.batur.rest.model;

public class Interval {

	private long id;
	private long intervalValue;
	
	/*
	 * interval object
	 */
	public Interval() {

	}
	public Interval(long id, long intervalValue) {
		this.id = id;
		this.intervalValue = intervalValue;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIntervalValue() {
		return intervalValue;
	}
	public void setIntervalValue(long intervalValue) {
		this.intervalValue = intervalValue;
	}
	@Override
	public String toString() {
		return "Interval [id=" + id + ", intervalValue=" + intervalValue + "]";
	}

	


}
