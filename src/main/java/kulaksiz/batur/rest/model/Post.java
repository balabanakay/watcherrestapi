package kulaksiz.batur.rest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="POST")
public class Post {
	
	@Id
	@Column (name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column (name="severity_type")
	private String severityType;
	
	@Column (name="severity_count")
	private int severityCount;

	/*
	 * Represent post object int the DB
	 * id, severity_type, severity_column
	 */
	public Post() {

	}
	public Post(String severityType, int severityCount) {
		this.severityType = severityType;
		this.severityCount = severityCount;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSeverityType() {
		return severityType;
	}
	public void setSeverityType(String severityType) {
		this.severityType = severityType;
	}
	public int getSeverityCount() {
		return severityCount;
	}
	public void setSeverityCount(int severityCount) {
		this.severityCount = severityCount;
	}
	@Override
	public String toString() {
		return "Post [id=" + id + ", severityType=" + severityType + ", severityCount=" + severityCount + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + severityCount;
		result = prime * result + ((severityType == null) ? 0 : severityType.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (id != other.id)
			return false;
		if (severityCount != other.severityCount)
			return false;
		if (severityType == null) {
			if (other.severityType != null)
				return false;
		} else if (!severityType.equals(other.severityType))
			return false;
		return true;
	}
	
	
	

}